
module.exports = {
    //use query to get local path
    get_index: function (lp) {
        var localPath = 'static/' + lp;
        var Promise = require('bluebird');
        var readdir = Promise.promisify(require('fs').readdir);
        var path = require('path');
        var statSync = require('fs').statSync;

        return readdir(localPath)
            .then((files) => {
                var _files = [];
                files.forEach(function (name) {
                    var filePath = path.join(localPath, name);
                    var stat = statSync(filePath);
                    if (stat.isFile()) {
                        _files.push({ path: name, type: 'file' })
                        //callback(filePath, stat);
                    } else if (stat.isDirectory()) {
                        _files.push({ path: name, type: 'folder' })
                    }
                });
                return _files;
            })
            .catch(function (err) {
                return [];
            })
    },
    delete_index: function (lp, type) {
        var localPath = 'static/' + lp;        
        var Promise = require('bluebird');        
        var removeFile = Promise.promisify(require('fs').unlink);
        var removeFolder = Promise.promisify(require('rimraf'));
        if(type=='folder') return removeFolder(localPath).then(function(){return true;}).catch(function(err) {return false});
        return removeFile(localPath).then(function(){return true;}).catch(function(err) {return false});        
    },
    post_index: function(oldPath, newPath){
        var _oldPath = 'static/' + oldPath;
        var _newPath = 'static/' + newPath;
        if(_oldPath == _newPath) return true;
        console.log(_oldPath);
        console.log(_newPath);
        var Promise = require('bluebird');
        var rename = Promise.promisify(require('fs').rename);
        return rename(_oldPath,_newPath)
            .then(function(){
                return true;
            })
            .catch(function(err) {
                console.log(err);
                return false;
            });
    }
}
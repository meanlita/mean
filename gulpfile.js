const path = require('path');
const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const ngAnnotate = require('gulp-ng-annotate');
const rename = require('gulp-rename');
const inject = require('gulp-inject');
const clean = require('gulp-clean');
const folders = require('gulp-folders');
const sourceMap = require('gulp-sourcemaps');
const htmlmin = require('gulp-htmlmin');
const nodemon = require('gulp-nodemon');
const browserSync = require('browser-sync');

const runSequence = require('run-sequence');
var exec = require('child_process').exec;

const BROWSER_SYNC_RELOAD_DELAY = 1500;
const FRONTEND_PATH = 'static/front-end';
const DB_PATH = 'C:/data/db/';

/*
    function runCommand(command) {
    return function (cb) {
        exec(command, function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
        });
    }
    }

    gulp.task('start-mongo', runCommand('mongod --dbpath ' + DB_PATH));
    gulp.task('stop-mongo', runCommand('mongo --eval "use admin; db.shutdownServer();"'));
*/

gulp.task('clean', function () {
    return gulp.src('static/user', { read: false }).pipe(clean());//clean all
});

gulp.task('buildJs', folders(FRONTEND_PATH, function (folder) {
    return gulp.src(path.join(FRONTEND_PATH, folder, '**/*.js'))
        .pipe(concat(folder + '.js'))
        .pipe(ngAnnotate())
        .pipe(sourceMap.init())
        .pipe(uglify({ mangle: true }))
        .pipe(rename({ extname: '.min.js' }))
        .pipe(sourceMap.write('.'))
        .pipe(gulp.dest('static/user'));
    }
));

gulp.task('minifyLib', function(){
    return gulp.src('static/js/textAngular/*.min.js').pipe(concat('textAngularBundle.js')).pipe(uglify()).pipe(gulp.dest('static/js/textAngular'));
});

gulp.task('injectJs', function () {
    return gulp.src('static/front-end/index.html')
        .pipe(inject(gulp.src('static/user/*.js',
            { read: false }),
            {
                transform: function (filepath) {
                    return '<script src="' + filepath.slice(13) + '"></script>';
                }
            }
        ))
        .pipe(gulp.dest('static/user'));
});

gulp.task('minifyViews', folders(FRONTEND_PATH, function (folder) {
    return gulp.src(path.join(FRONTEND_PATH, folder, 'views/*.html'))        
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('static/user/' + folder + '/views'));
}));

gulp.task('build', function (cb) {
    runSequence(['buildJs', 'minifyViews'], 'injectJs', cb);
});

gulp.task('rebuild', function(){
    runSequence('clean', 'build');
});

gulp.task('nodemon', function (cb) {
    var called = false;
    return nodemon({
        script: 'server.js',
        watch: ['api/**/*.js', 'controllers/**/*.js', 'server.js', 'security.js', 'setRoutes.js'],
        env: { 'NODE_ENV': 'development' }
    }).on('start', function onStart() {
        // ensure start only got called once
        if (!called) { cb(); }
        called = true;
    }).on('restart', function onRestart() {
        // reload connected browsers after a slight delay
        setTimeout(function () {
            browserSync.reload({
                stream: false
            });
        }, BROWSER_SYNC_RELOAD_DELAY);
    });
});

gulp.task('browser-sync', ['nodemon'], function () {
    setTimeout(function () {
        browserSync({
            proxy: 'http://localhost:3000',
            port: 4000
        });
    }, 5000);
});

gulp.task('syncCSS', function () {
    return gulp.src('static/**/*.css')
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('startMongo', function () {
    exec('mongod --dbpath ' + DB_PATH, function (err, stdout, stderr) {
        console.log(stdout);
    });
});

gulp.task('tracking', ['trackFrontEnd'], function () {
    gulp.watch('static/user/*.js', function (event) {
        if (event.type === 'added' || event.type === 'deleted' || event.type === 'renamed') runSequence('injectJs', browserSync.reload);
    });
    gulp.watch('static/front-end/index.html', ['injectJs', browserSync.reload]);
    gulp.watch('static/css/**/*.css', ['syncCSS']);
});

//for setting up development environment
gulp.task('default', function () {
    runSequence('startMongo', 'build', 'tracking', 'browser-sync');
});

//to make front-end production
gulp.task('start', function () {
    runSequence('clean', 'default');
});

gulp.task('trackFrontEnd', function () {
    gulp.watch('static/front-end/**/*.*', function (event) {
        var tmp = event.path.replace(__dirname, "").split("\\");
        var fileType = tmp[tmp.length - 1].split('.');
        if (fileType.length > 1) {
            if (fileType[fileType.length - 1].trim().toLowerCase() === 'html' && tmp.length > 4) {//minify view
                var destFile = 'static/user/' + tmp[3] + '/views';
                if (event.type == 'added' || event.type == 'changed' || event.type == 'renamed') {
                    var stream = gulp.src(event.path)
                        .pipe(htmlmin({ collapseWhitespace: true }))
                        .pipe(gulp.dest(destFile));
                    stream.on('end', function () {
                        browserSync.reload({
                            stream: false
                        });
                    });

                    stream.on('error', function (err) {
                        console.log(err);
                    });
                } else {//with delete event we do not reload browser
                    destFile = destFile + '/' + tmp[tmp.length - 1];
                    gulp.src(destFile).pipe(clean());
                }
            } else {
                if (fileType[fileType.length - 1].trim().toLowerCase() === 'js') {
                    var stream = gulp.src('static/front-end/' + tmp[3] + '/**/*.js')
                        .pipe(concat(tmp[3] + '.js'))
                        .pipe(ngAnnotate())
                        .pipe(sourceMap.init())
                        .pipe(uglify({ mangle: true }))
                        .pipe(rename({ extname: '.min.js' }))
                        .pipe(sourceMap.write('.'))
                        .pipe(gulp.dest('static/user'));
                    stream.on('end', function () {
                        browserSync.reload({
                            stream: false
                        });
                    });
                    stream.on('error', function (err) {
                        console.log(err);
                    });
                }
            }
        }
    });
});
bApp.controller('bookCtrl', function($scope, bookList, $uibModal, $http, taOptions){
    
    $scope.uploadhandler = function() {
        alert('running');
    };

    $scope.filedropped =  function(file, insertAction){        
        console.log('file dropping fired');
        //queue files and submit all
        //console.log(file);
        //console.log(insertAction);
        //insertAction('delete');
        //insertAction('insertImage', 'https://angular.io/resources/images/logos/angular2/angular.svg', false);
        //console.log($scope.htmlContent);

        return true;//to disable insert image when drop
        //add attribute to texAngular element ta-file-drop="filedropped"
    };


    $scope.books = bookList;
    $scope.addBook = function(){
        $scope.edit({}, -1);
    }
    $scope.edit = function(b, index){
        var editAddModal = $uibModal.open({
            animation: true,
            component: 'addEditBook',
            resolve: {
                book: function(){
                    var book = JSON.parse(JSON.stringify(b));
                    return b;
                }
            }
        });

        editAddModal.result.then(function (b) {
            if(index>=0) {
                $scope.books[index] = b;
            } else {
                $scope.books.unshift(b);
            }
        }, function () {/*dismiss*/});
    }
    $scope.delete = function(b, index){
        $http.delete('/api/book/index/' + b._id).then( 
        function(res){
            console.log(res.data);
            $scope.books.splice(index, 1);
        }, //scucess
        function(res){
            console.log(res.data);
        }); //error
    }
});
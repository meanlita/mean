var hub = angular.module('hub', ['ngAnimate', 'ui.select', 'ngSanitize', 'angular-locker', 'angular-jwt', 'ui.router', 'ui.bootstrap',
    'ngFileUpload', 'bApp', 'textAngular'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider, lockerProvider,
        $httpProvider, jwtOptionsProvider, $provide) {//, scrollableTabsetConfigProvider
        lockerProvider.defaults({
            driver: 'local',//or session
            namespace: 'hub',
            separator: '.',
            eventsEnabled: true,
            extend: {}
        });

        jwtOptionsProvider.config({
            tokenGetter: ['options', function (options) {
                // Skip authentication for any requests ending in .html
                if (options && options.url.substr(options.url.length - 5) == '.html') return null;

                //to eliminate double quote in token
                var token = localStorage.getItem('hub.token') ? localStorage.getItem('hub.token').replace(/(^\")|("$)/gi, "") : null;
                return token;
            }],
            unauthenticatedRedirector: ['$state', function ($state) {
                $state.go('hub.login');
            }]
        });

        $httpProvider.interceptors.push('jwtInterceptor');

        // this demonstrates how to register a new tool and add it to the default toolbar
        $provide.decorator('taOptions', ['taRegisterTool', '$delegate', '$uibModal', '$http', function (taRegisterTool, taOptions, $uibModal, $http) {
            // $delegate is the taOptions we are decorating
            //added uploadpath to textAngular.min.js at line 1055
            //https://github.com/textAngular/textAngular/issues/139
            //https://github.com/textAngular/textAngular/issues/54
            taRegisterTool('uploadImg', {
                iconclass: "fa fa-image",
                action: function (deferred, restoreSelection) {
                    var _uploadPath = this.$editor().uploadpath;
                    // var textNgEditor = this.$editor();
                    // textNgEditor.wrapSelection('insertImage', 'https://i.stack.imgur.com/WDxRB.jpg?s=64&g=1', true);
                    var modalInstance = $uibModal.open({
                        animation: true,
                        component: 'mediaComponent',
                        size: 'lg',
                        resolve: {
                            path: function () { return _uploadPath; }
                        }
                    }).result.then(
                        function (result) {
                            restoreSelection();
                            //document.execCommand('insertImage', true, 'https://i.stack.imgur.com/WDxRB.jpg?s=64&g=1');
                            document.execCommand('insertImage', true, result);
                            deferred.resolve();
                        },
                        function () {
                            deferred.resolve();
                        }
                        );
                    return false;
                }
            });

            taRegisterTool('_insertLink', {
                iconclass: "fa fa-link",
                action: function (deferred, restoreSelection) {
                    var _uploadPath = this.$editor().uploadpath;
                    var modalInstance = $uibModal.open({
                        animation: true,
                        component: 'mediaComponent',
                        size: 'lg',
                        resolve: {
                            path: function () { return _uploadPath; }
                        }
                    }).result.then(
                        function (result) {
                            restoreSelection();
                            document.execCommand('createlink', true, result);
                            deferred.resolve();
                        },
                        function () {
                            deferred.resolve();
                        }
                        );
                    return false;
                }
            });


            // taRegisterTool('colourRed', {
            //     iconclass: "fa fa-square red",
            //     action: function () {
            //         this.$editor().wrapSelection('forecolor', 'red');
            //     }
            // });            
            // taOptions.toolbar[1].push('colourRed');

            taOptions.defaultFileDropHandler = function (file, insertAction) {
                var _uploadPath = this.uploadpath;
                var data = new FormData();
                data.append("file", file);
                $http.post('/upload?lp=' + _uploadPath, data, {
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function (res) {
                    var url = res.data[0];
                    insertAction('insertImage', '/' + url, true);
                }, function (res) {
                    console.log(res.status);
                });

                return false;//return true to disable insert image when drop
            };

            taOptions.toolbar = [['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'undo', 'redo', 'clear',
                'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'indent', 'outdent',
                'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote', 'uploadImg', '_insertLink', 'insertVideo', 'html'],
            ];            
            return taOptions;
        }]);

        //config scrollable tabs        
        // scrollableTabsetConfigProvider.setShowTooltips(true);
        // scrollableTabsetConfigProvider.setTooltipLeftPlacement('bottom');
        // scrollableTabsetConfigProvider.setTooltipRightPlacement('left');

        $stateProvider.state({
            name: 'hub',
            url: '/',
            templateUrl: 'hub/views/main.html',
            controller: 'MainCtrl',
            onEnter: function () {
                console.log('main ctrl is initialized');
            }
        })
            .state({
                name: 'hub.login',
                url: 'login',
                templateUrl: 'hub/views/loginForm.html',
                controller: 'LoginCtrl'
            })
            .state({
                name: 'hub.frontends',
                url: 'frontend',
                templateUrl: 'hub/views/frontend.html',
                controller: 'FrontendCtrl',
                resolve: {
                    frontendList: function (getList) {
                        return getList.getAll('/api/frontend');
                    }
                }
            })
            .state({
                name: 'hub.backends',
                url: 'backend',
                templateUrl: 'hub/views/backend.html',
                controller: 'BackendCtrl',
                resolve: {
                    backendList: function (getList) {
                        return getList.getAll('/api/backend');
                    }
                }
            })
            .state({
                name: 'hub.roles',
                url: 'role',
                templateUrl: 'hub/views/role.html',
                controller: 'RoleCtrl',
                resolve: {
                    roleList: function (getList) {
                        return getList.getAll('/api/role');
                    },
                    frontendList: function (getList) {
                        return getList.getAll('/api/frontend/activated');
                    },
                    backendList: function (getList) {
                        return getList.getAll('/api/backend/activated');
                    }
                }
            })
            .state({
                name: 'hub.users',
                url: 'management',
                templateUrl: 'hub/views/user.html',
                controller: 'UserCtrl',
                resolve: {
                    userList: function (getList) {
                        return getList.getAll('/api/user');
                    },
                    roleList: function (getList) {
                        return getList.getAll('/api/role/activated');
                    }
                }
            });



        $locationProvider.html5Mode({ enabled: true, requireBase: true });
    }).run(function (authManager, $trace) {//, $transitions, jwtHelper, locker, $state, $trace, $rootScope) {
        console.log('app is running');
        $trace.enable('TRANSITION');

        //get local private information
        //privateInfo.bind();
        //console.log(jwtHelper.getTokenExpirationDate($rootScope.token));
        //console.log(typeof(jwtHelper.getTokenExpirationDate($rootScope.token)));
        //console.log(jwtHelper.getTokenExpirationDate($rootScope.token) < Date.now());
        //console.log(jwtHelper.isTokenExpired($rootScope.token));
        //if ($rootScope.token && jwtHelper.getTokenExpirationDate($rootScope.token) < Date.now()) {//recover app from previous session
        // if ($rootScope.token) {
        //     privateInfo.bind();
        //     if ($rootScope.menus && $rootScope.menus.length && $rootScope.username && $rootScope.fullname) {
        //         if ($rootScope.tabs === undefined) { $rootScope.tabs = []; $rootScope.activeTabIndex = -1; }
        //         else {//sync tabs and menus
        //             $rootScope.tabs.forEach(function (tab, index) {
        //                 var n = $rootScope.menus.length;
        //                 for (var i = 0; i < n; i++) {
        //                     if ($rootScope.menus[i].url && $rootScope.menus[i].url == tab.url) {
        //                         $rootScope.tabs[index] = $rootScope.menus[i];
        //                         if ($rootScope.currentMenu.url == $rootScope.tabs[index].url) {
        //                             $rootScope.currentMenu = $rootScope.menus[i];
        //                             $rootScope.activeTabIndex = index;
        //                         }
        //                         break;
        //                     }
        //                     else {
        //                         var breakable = false;
        //                         if ($rootScope.menus[i].frontends && $rootScope.menus[i].frontends.length) {
        //                             var l = $rootScope.menus[i].frontends.length;
        //                             for (var j = 0; j < l; j++) {
        //                                 if ($rootScope.menus[i].frontends[j].url == tab.url) {
        //                                     $rootScope.tabs[index] = $rootScope.menus[i].frontends[j];
        //                                     if ($rootScope.currentMenu.url == $rootScope.tabs[index].url) {
        //                                         $rootScope.currentMenu = $rootScope.tabs[index];
        //                                         $rootScope.activeTabIndex = index;
        //                                     }
        //                                     breakable = true;
        //                                     break;
        //                                 }
        //                             }
        //                         }
        //                         if (breakable) break;
        //                     }
        //                 }
        //             });
        //         }
        //     } else {//private information is not integrity ==>clear
        //         console.log('clear because integrity error');
        //         privateInfo.clear();
        //     }
        // } else {//clear private information
        //     console.log('clear because token expired');
        //     privateInfo.clear();
        // }

        // $transitions.onSuccess({}, function (trans){
        //     //add tab if enter url
        //     //console.log(trans._targetState._options.source);
        //     //console.log('transition success');
        //     //console.log(targetState);

        // });

        //$rootScope.$on("$stateChangeError", console.log.bind(console));

        //must check token. If token not exist or expired => redirect to login state with a redirectState param.
        //$transitions.onStart({}, function (trans) {
        // console.log('event start state is fired');
        // console.log(trans._targetState._identifier.name);
        // console.log(trans._targetState._options.source);
        //var targetState = typeof(trans._targetState._identifier) == 'string' ? trans._targetState._identifier : trans._targetState._identifier.name;
        // if(!(targetState == 'hub' || targetState == 'hub.login')) { //check authentication
        //     var token = locker.get('token');
        //     var date = jwtHelper.getTokenExpirationDate(token);
        //     console.log(date);
        //     console.log(Date.now());
        //     if(Date.now() < date) console.log('not expired');

        //     // console.log(token);
        //     // if(token && jwtHelper.isTokenExpired(token)) {
        //     //     console.log(token);
        //     // } else {
        //     //     var date = jwtHelper.getTokenExpirationDate(token);
        //     //     if(Date.now < date) console.log('not expired');
        //     //     console.log(date);
        //     //     console.log(jwtHelper.isTokenExpired(token));
        //     //     return trans.router.stateService.target('hub.login')
        //     // }
        // }



        //console.log(n);
        //console.log(i);
        //console.log(trans);
        //console.log(trans._targetState._identifier.name);
        //_options.source ="url" or "unknown"
        //if(typeOf(_targetState) =='ie') ==> enter url. otherwise _identifier just is string
        //onStart, onSuccess, onError, onEnter, onExit = function(t,n,i);

        //redirect to anther state: return trans.router.stateService.target('login');
        //inject service to $transitions: var auth = trans.injector().get('AuthService');
        //});
        //editableOptions.theme = 'bs3';
        authManager.redirectWhenUnauthenticated();
    })
    .service('getList', function ($http, $q) {
        this.getAll = function (url) {
            var deferred = $q.defer();
            $http.get(url).then(function (res) {
                deferred.resolve(res.data);
            }, function (res) {
                deferred.reject(res.status);
            });
            return deferred.promise;
        }
    })
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .component('addReasonComponent', {
        template: '<div class="modal-header">'
        + '<h3 class="modal-title" id="modal-title">{{$ctrl.activated ? "Disable" : "Activate"}} {{$ctrl.title}}</h3></div>'

        + '<div class="modal-body" id="modal-body">'
        + '<textarea class="form-control" placeholder="Reasons why you disable or active this function" rows="3" ng-model="$ctrl.reason"></textarea></div>'
        + '<div class="modal-footer">'
        + '<button class="btn btn-primary" type="button" ng-click="$ctrl.ok()">{{$ctrl.activated ? "Disable" : "Activate"}}</button>'
        + '<button class="btn btn-warning" type="button" ng-click="$ctrl.cancel()">Cancel</button></div>',
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        },
        controller: function () {
            var $ctrl = this;

            $ctrl.$onInit = function () {
                $ctrl.activated = $ctrl.resolve.activated;
                $ctrl.title = $ctrl.resolve.title;
            };

            $ctrl.ok = function () {
                $ctrl.close({ $value: $ctrl.reason });
            };

            $ctrl.cancel = function () {
                $ctrl.dismiss({ $value: 'cancel' });
            };
        }
    })
    .component('mediaComponent', {
        templateUrl: 'hub/views/media.html',
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        },
        controller: function ($http) {
            var $ctrl = this;
            $ctrl.files = [];
            var getClassType = function (filename) {
                var fileType = filename.substr(filename.lastIndexOf('.') + 1).trim().toUpperCase();
                var classType = "";
                switch (fileType) {
                    case 'PNG':
                    case 'GIF':
                    case 'JPG':
                    case 'JPEG':
                    case 'BMP':
                    case 'TIFF':
                        classType = 'fa-file-image-o';
                        break;
                    case 'DOCX':
                    case 'DOC':
                        classType = 'fa-file-word-o';
                        break;
                    case 'XLSX':
                    case 'XLS':
                        classType = 'fa-file-excel-o';
                        break;
                    case 'PPTX':
                    case 'PPT':
                        classType = 'fa-file-powerpoint-o';
                        break;
                    case 'PDF':
                        classType = 'fa-file-image-o'
                        break;
                    case 'AVI':
                    case 'MOV':
                    case 'MP4':
                    case 'MPEG':
                    case 'WMV':
                    case 'FLV':
                    case 'SWF':
                        classType = 'fa-file-video-o';
                        break;
                    case 'ZIP':
                    case 'RAR':
                        classType = 'fa-file-archive-o';
                    default:
                        classType = 'fa-file';
                        break;
                }
                return classType;
            }
            var getFileInfor = function () {
                $ctrl.files = [];
                $http.get('/api/media?lp=' + $ctrl.path).then(function (res) {
                    res.data.forEach(function (file) {
                        file.class = 'fa-folder';
                        file.name = file.path;
                        if (file.type == 'file') {
                            var _idx = file.path.indexOf('_');
                            file.id = file.path.substr(0, _idx);
                            file.name = file.path.substr(_idx + 1);
                            file.class = getClassType(file.name);
                        }
                    });

                    $ctrl.files = res.data.sort(function (a, b) { return a.type < b.type });
                }, function (res) { });
            }

            $ctrl.$onInit = function () {
                $ctrl.path = $ctrl.resolve.path;
                getFileInfor();
            };

            $ctrl.createNewFolder = function () {
                $ctrl.files.unshift({ type: 'folder', class: 'fa-folder', editable: true });
            }

            $ctrl.rename = function (f) {
                f.newName = f.name;
                f.editable = true;
            }

            $ctrl.delete = function (f) {
                $http.delete('/api/media?lp=' + $ctrl.path + '/' + f.path + '&type=' + f.type).then(function (res) {
                    if (res.data) {
                        var fIdx = $ctrl.files.indexOf(f);
                        $ctrl.files.splice(fIdx, 1);
                    }
                }, function (res) {
                    console.log(res.status);
                })
            }

            $ctrl.changeFName = function (f) {
                if (f.type == 'folder' && f.name == undefined) {
                    if (f.newName && f.newName.trim()) {
                        f.name = f.newName.trim();
                        f.path = f.name;
                        f.editable = false;
                    } else {
                        $ctrl.files.splice($ctrl.files.indexOf(f), 1);
                    }
                } else if (f.newName && f.newName.trim() && f.name != f.newName.trim()) {
                    f.newName = f.newName.trim();

                    var fileExt = f.newName.lastIndexOf('.');
                    if (f.type == 'file' && (fileExt < 0 || fileExt == f.newName.length - 1)) {
                        var ext = f.name.substr(f.name.lastIndexOf('.') + 1);
                        if (fileExt < 0) f.newName = f.newName + '.' + ext;
                        else f.newName = f.newName + ext;
                    }

                    f.newPath = f.newName;

                    if (f.type == 'file') {
                        f.newPath = f.id + '_' + f.newName;
                    }

                    $http.post('/api/media', { oldPath: $ctrl.path + '/' + f.path, newPath: $ctrl.path + '/' + f.newPath }).then(function (res) {
                        if (res.data) {
                            f.path = f.newPath;
                            f.name = f.newName;
                            f.newName = null;
                            f.newPath = null;
                            f.editable = false;
                        }
                    }, function (res) { })
                } else {
                    f.newName = null;
                    f.newPath = null;
                    f.editable = false;
                }
            }

            $ctrl.selected = function (f) {
                if (f.type == 'file') {
                    $ctrl.selectedUrl = '/img/' + f.path;
                    $ctrl.close({ $value: $ctrl.selectedUrl });
                } else {
                    $ctrl.path = $ctrl.path + '/' + f.path;
                    getFileInfor();
                }
            }

            $ctrl.select = function (f) {
                if (f.type == 'file') {
                    $ctrl.selectedUrl = '/' + $ctrl.path + '/' + f.path;
                }
            }

            $ctrl.dropFileHandler = function ($files) {
                var data = new FormData();
                $files.forEach(function (f, index) {
                    data.append('file' + index, f);
                });

                $http.post('/upload?lp=' + $ctrl.path, data, {
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function (res) {
                    res.data.forEach(function (filePath) {
                        var filenameParts = filePath.split('/');
                        var filename = filenameParts[filenameParts.length - 1];
                        var idx = filename.indexOf('_');
                        var file = {
                            id: filename.substr(0, idx),
                            path: filePath,
                            type: 'file',
                            name: filename.substr(idx + 1),
                            class: getClassType(filename)
                        }
                        $ctrl.files.unshift(file);
                    })
                }, function (res) {
                    console.log(res.status);
                });
            }

            $ctrl.ok = function () {
                $ctrl.close({ $value: $ctrl.selectedUrl });
            };

            $ctrl.cancel = function () {
                $ctrl.dismiss({ $value: 'cancel' });
            };
        }
    });
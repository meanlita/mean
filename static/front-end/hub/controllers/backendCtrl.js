hub.controller('BackendCtrl', function ($scope, backendList, $uibModal, $http, $window) {
    $scope.backendList = backendList;
    $scope.add = function () {
        var newBackend = {};
        $scope.edit(newBackend, -1);
    };

    $scope.changeState = function(f){
        var modalInstance = $uibModal.open({
            animation: true,
            component: 'addReasonComponent',
            resolve: {
                activated: function () {
                    return f.activated;
                },
                title: function(){
                    return f.title;
                }
            }
        });

        modalInstance.result.then(function (reason) {
            $http.put('/api/funcLog', {log: {
                funcId: f._id,
                funcType:'backend',
                reason: reason,
                activated: !f.activated,
                username: $window.sessionStorage.username,
            }}).then(function(res){
                f.activated = !f.activated;
            }, function(res){//error
            });
        }, function () {/*dismiss*/});
    }

    $scope.edit = function(f, index){
        var editAddModal = $uibModal.open({
            animation: true,
            component: 'addEditBackend',
            resolve: {
                backend: function(){
                    var backend = JSON.parse(JSON.stringify(f));
                    return backend;
                }
            }
        });

        editAddModal.result.then(function (backend) {
            if(index>=0) {
                $scope.backendList[index] = backend;
            } else {
                $scope.backendList.unshift(backend);
            }
        }, function () {/*dismiss*/});
    }
});

hub.component('addEditBackend', {
    templateUrl: 'hub/views/addEditBackend.html',
     bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: function($http){
        var $ctrl = this;

        $ctrl.$onInit = function () {
            $ctrl.backend = $ctrl.resolve.backend;  
        };

        $ctrl.ok = function () {
            if($ctrl.backend._id) {//update
                $http.post('/api/backend', {backend: $ctrl.backend}).then(function(res){
                    $ctrl.close({$value: $ctrl.backend});
                }, function(res){
                    console.log(res);
                });
            } else {//create new frontend function
                $http.put('/api/backend', {backend: $ctrl.backend}).then(function(res){
                    $ctrl.close({$value: res.data});
                }, function(res) {})
            }
        };

        $ctrl.cancel = function () {
            $ctrl.dismiss({$value: 'cancel'});
        };
    }
})
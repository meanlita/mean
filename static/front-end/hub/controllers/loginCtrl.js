hub.controller('LoginCtrl', function ($scope, $http, $state, locker) {
    $scope.$parent.tabs = undefined;
    $scope.$parent.activeTabIndex = undefined;

    $scope.loginFormClass = 'login';
    $scope.state = 'Login';

    $scope.login = function () {
        $scope.loginFormClass = 'login loading';
        $scope.state = 'Đang xác thực';

        $http.post('/api/login', { username: $scope.username, password: $scope.password })
            .then(function (res) {
                $scope.loginFormClass = 'login loading ok';
                $scope.state = 'Welcome back';
                $http.defaults.headers.common.Authorization = 'Bearer ' + res.data.token;
                locker.put('token', res.data.token);
                locker.put('username', res.data.user.username);
                locker.put('fullname', res.data.user.fullname);

                var menus = [];
                
                if (res.data.user.roles.length === 1) menus = res.data.user.roles[0].frontends;
                else {
                    res.data.user.roles.forEach(function (role, index) {
                        if (role.frontends.length === 1) {
                            menus.push(role.frontends);
                        } else {
                            menus.push(role);
                        }
                    });
                }

                locker.put('menus', menus);                

                $state.go('hub', {}, { reload: true });

            }, function (res) {//reset form login
                $scope.loginFormClass = 'login';
                $scope.state = 'Login';
            });
    }
});